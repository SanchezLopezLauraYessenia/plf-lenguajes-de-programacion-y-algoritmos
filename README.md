# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?
```plantuml
@startmindmap
*[#f08471] Programando ordenadores\n   en los 80 y ahora
	*[#f2fb69] Sistema
		*_ Se divide en:
			*[#9bf0a2] Sistemas antiguos:
				*_ Son:
					* Sistema que no haya\ntenido una continuidad a \ngeneraciones posteriores.
				
				*[#71f0ad] Programación en \nlos 80's y 90's.
					* Era necesario conocer la\n arquitectura del hardware de cada máquina.
					* El hardware ponía las limitaciones 
					* Los cuellos de botellas era de hardware
				*[#9bf0a2] Sistemas actuales:
					*_ Caracteristicas
						* Se tiene en casa\no se puede adquirir\nen alguna tienda
						* Si tienes idea de cómo programar una \nmáquina puedes programar otra

	*[#f2fb69] Mejoras:
		*_ Han sido
			* Todas las máquinas son muy parecidas
			* El programador no tiene que conocer cada plataforma
			* Precios más accesibles.

	*[#f2fb69] Diferencias de Antes y Ahora.
		*_ Son:
			* La mejor potencia en las máquinas actuales
			* La velocidad en los procesadores.				
			* Existe más conocimiento \nsobre las arquitectura.
			* El tiempo no afecta\ngradualmente su\nrendimiento.

	*[#f2fb69] ¿Que es Programar? 
		* Decirle a la máquina lo que se quiere que haga
		*_  Tipos de lenguajes
			*[#9bf0a2] Alto nivel
				*[#71f0ad] Tienen una traducción\n a código máquina.
				*[#71f0ad] Actualmente lo realizan\nlos compiladores\nde intérpretes
				*[#71f0ad] Contras
					* Los compiladores no son tan eficientes \ncomo los que se generan en ensamblador.	
					* Los programadores desconocen \nel funcionamiento del hardware.
					* Sobrecarga debido a las capas
				*[#71f0ad] Ejemplos
					* C++
					* Fortran
					* JAVA
			*[#9bf0a2] Bajo nivel.
				*[#71f0ad] Estos lenguajes están orientados a procesos
				*[#71f0ad] Lenguaje ensamblador
					* Con este se provecha bien los recursos
@endtmindmap
```
# Historia de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
*[#e2c6fc] Historia de los algoritmos y de \nlos lenguajes de programación
	*[#c6e2fc] Algoritmos
		*_ ¿Qué es?
			* Lista definida, ordenada y  finita de operaciones \nque hallan la solución a un problema.
				*_ Se divide
					*[#8af67b] Razonables
						* Tiempo de ejecución crece despacio a medida \nque los problemas se hacen más grandes
					*[#8af67b] No razonables
						* Añadir un dato más al problema \n hace que se duplique el tiempo
				*_ Ejemplo
					* Manuales de usuario


		*_ Historia
			*[#98f9d1]  3000 antes de Cristo 
				* Tablillas de arcilla para \ncalcular el capital
			*[#98f9d1]  Siglo XVII 
				* Primeras ayudas mecánicas \nen forma de calculadoras 
			*[#98f9d1]  Siglo XIX  
				* Primeras máquinas programables. 


		*_ Importancia
			* Realizar procesos y resolver mecánicamente \nproblemas matemáticos o de otro tipo 

	*[#c6e2fc] Lenguajes de Programación 
		*[#fc305e] Paradigma
			* Manera radical distinta \nde concebir los cómputos

				*[#b6b2f9] Paradigma Funcional
					* Trabajan con la idea \nde la simplificación
						*_ Años 60s \nAparece
							*[#e0e31d] Lisp
								*_ otros
									* Haskell
									* Miranda
									* Erlang

				*[#b6b2f9] Orientados a objetos
					* Modelo de programación que organiza \nel diseño en torno a datos u objetos
						*[#e0e31d] 1968 Aparece\n Simula
							*_ otros
								* Python
								* PHP
								* Ruby

				*[#b6b2f9] Programación lógica
					* Computa a base \nde fórmulas lógicas
						*[#e0e31d] 1971 Aparaece\n Prolog
							*_ otros
								* Mercury
								* Gödel
								* ALF

		*[#fc305e] Lenguajes más influyentes
			* JAVA
			* C++
			* Fortran

	*[#c6e2fc] Programa 
		*[#f8eed4] Operaciones especificadas en un \ndeterminado lenguaje de programación
@endtmindmap
```

# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#fbd74f] La evolución de los lenguajes \ny paradigmas de programación
	*[#fb456e] Conceptos Fundamentales
		* Origen Computadora 
			*_ Se dio para Facilitar el trabajo intelectual 
		* Lenguajes de programación:
			*_ Medio de comunicación entre \nel humano y la máquina.
		* Paradigma:
			*_ Forma de pensar o abordar \nel desarrollo de un programa
		* Máquinas:
			*_ Son codificadas en lenguaje binario

	*[#fb456e] Programación estructurada:
		*  Combinación de tres estructuras básicas \nes suficiente para expresar cualquier función computable.
		*_ Primer Lenguaje \nde este tipo
			* Algol
		*_ Se usa en:
			* Basic
			* C
			* Pascal
			* JAVA
			* Fortran

	*[#fb456e] Paradigmas:
		*[#69c6fb] Creación
			* Debido a fallas de la programación estructurada \nse crean otros paradigmas para corregir esto
		*[#69c6fb] Tipos
			*[#69fbb0] Funcional
				* Funciones matemáticas que \ndevuelven un número o letra
				* Recursividad hasta que \nse cumpla una condición
				* Lenguajes:
					* ML
					* HASKELL
					* ERLANG
			*[#69fbb0] Lógico
				* Uso de recursividad
				* Permite pensar en términos \nde premisas y conclusiones
				* Dominio de problemas mediante \nuna serie de axiomas lógicos
				* Lenguaje
					*_ Prolog
					*_ Mercury
					*_ ALF

			*[#69fbb0] Programación Concurrente
				* Expresar el paralelismo entre tareas
				* Lenguajes: 
					* Ada
					* Java

				* Ejemplos:
					* Una cuenta vacía no \npuede sacar dinero 
					* Transacciones simultaneas en una cuenta
			*[#69fbb0] Programación distribuida
				* Comunicación entre computadoras
				* Programas divididos en diversas computadoras
					* abiertos
					* escalables
					* transparentes
				* Lenguajes: 
					* Ada
					* E
					* Limbo
					* Oz
			*[#69fbb0] Programación orientada \nen componentes
				* Un componente es un conjunto de objetos
				* Utiliza la reutilización
			*[#69fbb0] Programación orientada en aspectos
				* Se agregan capas en base \na la necesidad del programa
				* Las capas deben ser \nindependientes permitiendo: 
					*_ Legibilidad 
					*_ Comodidad
			*[#69fbb0] Programación orientada \nagente software 
				* Agentes que forman parte de \nla programacion orientada a objetos
					*_ Entidades autónomas con \nel propósito de alcanzar sus objetivos
				* Multiagentes 
					* Revuelven grandes problemas
				* Ejemplo:
					* El agente busca recomendaciones con \nbase en los gustos del perfil del cliente
@endtmindmap
```
